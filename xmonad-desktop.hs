{-# OPTIONS_GHC -fno-warn-missing-signatures #-}

--------------------------------------------------------------------------------

-- | XMonad.hs
--
-- Gerry Agbobada's configuration file for xmonad using some of the latest recommended
-- features (e.g., 0.17 goodies such as ewmh and withEasySB).
module Main
  ( main,
  )
where

--------------------------------------------------------------------------------
import Data.Foldable (traverse_)
import Gagbo.Bindings.Qwerty (keyBindings)
import Gagbo.Config
  ( myLayouts,
    myManageHook,
    myModKey,
    myTerminal,
    myWorkspaces,
    myXMobarPP,
  )
import XMonad
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.StatusBar
  ( statusBarProp,
    withSB,
  )
import XMonad.Hooks.UrgencyHook
  ( UrgencyHook (..),
    withUrgencyHook,
  )
import qualified XMonad.StackSet as W
import XMonad.Util.EZConfig (additionalKeysP)
import qualified XMonad.Util.NamedWindows as W
import XMonad.Util.Run (safeSpawn)
import XMonad.Util.SpawnOnce (spawnOnce)
import XMonad.Hooks.ManageDocks (docks)

--------------------------------------------------------------------------------

-- | Customize Startup
myStartupHook :: X ()
myStartupHook = do
  spawnOnce "picom -b"
  spawnOnce "dunst"
  -- When using taffybar as a status bar
  -- spawnOnce "status-notifier-watcher" -- necessary for taffybar tray
  -- spawnOnce "gagbo-taffybar"
  -- When using polybar as a status bar
  -- spawn "~/.config/polybar/launch.sh"
  spawnOnce
    "trayer --edge top --align right --SetDockType true --SetPartialStrut true --expand true --width 5 --transparent true --tint 0x5f5f5f --height 25"
  spawnOnce "udiskie"
  spawnOnce "nextcloud"
  spawnOnce "nm-applet --sm-disable --indicator"
  spawnOnce "blueman-applet"

--------------------------------------------------------------------------------

-- | Start xmonad using the main desktop configuration with
-- my overrides:
main :: IO ()
main =
  xmonad $
    withUrgencyHook LibNotifyUrgencyHook $
      ewmhFullscreen $
        ewmh $
        docks $
          withSB
            (statusBarProp "xmobar-gagbo" (pure myXMobarPP))
            $ def
              { terminal = myTerminal,
                modMask = myModKey,
                normalBorderColor = "#0C0C14",
                focusedBorderColor = "#2DACB7",
                borderWidth = 2,
                startupHook = myStartupHook,
                manageHook = myManageHook,
                workspaces = myWorkspaces,
                layoutHook = myLayouts
              }
              `additionalKeysP` keyBindings

-- original idea: https://pbrisbin.com/posts/using_notify_osd_for_xmonad_notifications/
data LibNotifyUrgencyHook = LibNotifyUrgencyHook
  deriving (Read, Show)

instance UrgencyHook LibNotifyUrgencyHook where
  urgencyHook LibNotifyUrgencyHook w = do
    winName <- W.getName w
    maybeIdx <- W.findTag w <$> gets windowset
    traverse_
      (\i -> safeSpawn "notify-send" [show winName, "workspace " ++ i])
      maybeIdx
