-- |

module Gagbo.Util.DBusLogHook
  ( dbusLogHook
  , connectDBusXMonad
  , module XMonad.Hooks.DynamicLog
  )
where

import           Gagbo.Config
import           XMonad
import           XMonad.Hooks.DynamicLog

import qualified DBus                          as D
import qualified DBus.Client                   as D
import qualified Codec.Binary.UTF8.String      as UTF8

-- Override the PP values as you would otherwise, adding colors etc depending
-- on  the statusbar used
dbusLogHook :: D.Client -> PP
dbusLogHook dbus = def
  { ppOutput  = dbusOutput dbus
  , ppCurrent = wrap ("%{B" ++ bgCurrent ++ "}%{F" ++ fgCurrent ++ "} ")
                     " %{F-}%{B-}"
  , ppVisible = wrap ("%{B" ++ bg1 ++ "} ") " %{B-}"
  , ppUrgent  = wrap ("%{B" ++ urgent ++ "}%{F" ++ fgCurrent ++ "} ")
                     " %{F-}%{B-}"
  , ppHidden  = wrap " " " "
  , ppWsSep   = ""
  , ppSep     = "%{F" ++ (paletteCyan gagboPalette) ++ "}" ++ " : " ++ "%{F-}"
  , ppTitle   = shorten 40
  }
 where
  bg1       = paletteBg gagboPalette
  bgCurrent = paletteYellow gagboPalette
  fgCurrent = paletteBlack gagboPalette
  urgent    = paletteRed gagboPalette


connectDBusXMonad :: IO D.Client
connectDBusXMonad = do
  dbus <- D.connectSession
  _    <- D.requestName
    dbus
    (D.busName_ "org.xmonad.Log")
    [D.nameAllowReplacement, D.nameReplaceExisting, D.nameDoNotQueue]
  return dbus

-- Emit a DBus signal on log updates
dbusOutput :: D.Client -> String -> IO ()
dbusOutput dbus str = do
  let signal = (D.signal objectPath interfaceName memberName)
        { D.signalBody = [D.toVariant $ UTF8.decodeString str]
        }
  D.emit dbus signal
 where
  objectPath    = D.objectPath_ "/org/xmonad/Log"
  interfaceName = D.interfaceName_ "org.xmonad.Log"
  memberName    = D.memberName_ "Update"
