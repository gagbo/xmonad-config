-- | Customize the PrettyPrinting in Xmobar status bar

module Gagbo.Util.XMobarLogHook where

import           Gagbo.Config
import           XMonad
import           XMonad.Hooks.DynamicLog
import           XMonad.Util.SpawnNamedPipe
import           XMonad.Util.Run

xMobarLogHook :: String -> X ()
xMobarLogHook pipeName = do
  mh <- getNamedPipe pipeName
  dynamicLogWithPP myXMobarPP { ppOutput = maybe (\_ -> return ()) hPutStrLn mh
                              }
