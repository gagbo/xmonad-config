-- | Manipulate windows as they are created.  The list given to
-- @composeOne@ is processed from top to bottom.  The first matching
-- rule wins.

module Gagbo.Config.ManageHook where


import           Gagbo.Config.Misc
import           Gagbo.Config.NamedScratchpad
import           XMonad
import           XMonad.Config.Desktop
import           XMonad.Hooks.ManageDocks
import           XMonad.Hooks.ManageHelpers

--
-- Use the `xprop' tool to get the info you need for these matches.
-- For className, use the second value that xprop gives you.
myManageHook :: ManageHook
myManageHook =
  manageDocks
    <+> myWindowRules
    <+> manageHook desktopConfig
    <+> myNSManageHook scratchpads


myWindowRules :: ManageHook
myWindowRules =
  composeAll
    . concat
    $ [ [isDialog --> doCenterFloat]
      , [isFullscreen --> doFullFloat]
      , [ (className =? c <||> title =? c <||> resource =? c) --> doIgnore
        | c <- bars
        ]
      , [ (className =? c <||> title =? c <||> resource =? c) --> doFloat
        | c <- nfloat
        ]
      , [ (className =? c <||> title =? c <||> resource =? c) --> doCenterFloat
        | c <- cfloat
        ]
      , [ (className =? c <||> title =? c <||> resource =? c)
            --> doShift (myWorkspaces !! 0)
        | c <- dev
        ]
      , [ (className =? c <||> title =? c <||> resource =? c)
            --> doShift (myWorkspaces !! 1)
        | c <- www
        ]
      , [ (className =? c <||> title =? c <||> resource =? c)
            --> doShift (myWorkspaces !! 2)
        | c <- sys
        ]
      , [ (className =? c <||> title =? c <||> resource =? c)
            --> doShift (myWorkspaces !! 3)
        | c <- doc
        ]
      , [ (className =? c <||> title =? c <||> resource =? c)
            --> doShift (myWorkspaces !! 4)
        | c <- ws5
        ]
      , [ (className =? c <||> title =? c <||> resource =? c)
            --> doShift (myWorkspaces !! 5)
        | c <- chat
        ]
      , [ (className =? c <||> title =? c <||> resource =? c)
            --> doShift (myWorkspaces !! 6)
        | c <- mus
        ]
      , [ (className =? c <||> title =? c <||> resource =? c)
            --> doShift (myWorkspaces !! 7)
        | c <- vid
        ]
      , [ (className =? c <||> title =? c <||> resource =? c)
            --> doShift (myWorkspaces !! 8)
        | c <- ws9
        ]
      , [ role =? c --> doFloat | c <- im ]     -- place roles on im
      ]
 where
  bars   = ["xmobar", "dzen2", "desktop_window"]
  nfloat = ["mpv", "vlc", "XCalc", "KeePass2", "KeePassXC", "Pidgin"]
  cfloat = ["MPCWindow", "HTOPWindow"]
  dev    = ["Emacs", "Gvim", "Nvim-qt"]
  www    = ["Firefox", "Chromium"]
  sys    = ["HTOPWindow"]
  doc    = []
  ws5    = ["KeePassXC"]
  chat   = ["Konversation", "Discord"]
  mus    = ["MPCWindow"]
  vid    = ["vlc"]
  ws9    = []
  im     = ["nothing"]
  role   = stringProperty "WM_WINDOW_ROLE"
