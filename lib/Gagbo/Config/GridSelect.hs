-- |

module Gagbo.Config.GridSelect
  ( module Gagbo.Config.GridSelect
  , module XMonad.Actions.GridSelect
  ) where

import           Gagbo.Config.Misc
import           XMonad
import           XMonad.Actions.GridSelect
import           XMonad.Util.NamedScratchpad

spawnSelected' :: [(String, String)] -> X ()
spawnSelected' lst = gridselect conf lst >>= flip whenJust spawn
  where conf = (mygridConfig defaultColorizer)

scratchpadGridSelect :: [NamedScratchpad] -> [(String, String)] -> X ()
scratchpadGridSelect pads lst = gridselect conf lst
  >>= flip whenJust (namedScratchpadAction pads)
  where conf = (mygridConfig defaultColorizer)

myColorizer :: Window -> Bool -> X (String, String)
myColorizer = colorRangeFromClassName (0x31, 0x2e, 0x39) -- lowest inactive bg
                                      (0x31, 0x2e, 0x39) -- highest inactive bg
                                      (0x61, 0x57, 0x72) -- active bg
                                      (0xc0, 0xa7, 0x9a) -- inactive fg
                                      (0xff, 0xff, 0xff) -- active fg

-- gridSelect menu layout
mygridConfig colorizer = (buildDefaultGSConfig colorizer)
  { gs_cellheight   = 30
  , gs_cellwidth    = 200
  , gs_cellpadding  = 8
  , gs_originFractX = 0.5
  , gs_originFractY = 0.5
  , gs_font         = myFont
  }
