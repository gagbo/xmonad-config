{-# LANGUAGE PartialTypeSignatures #-}

-- | Customize layouts.
module Gagbo.Config.Layouts where

import Gagbo.Config.Misc
import XMonad
import XMonad.Config.Desktop
import XMonad.Hooks.ManageDocks
import XMonad.Layout.BinarySpacePartition
import XMonad.Layout.Decoration
import XMonad.Layout.Magnifier
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed
import XMonad.Layout.ResizableTile (ResizableTall (..))
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns
import XMonad.Util.Themes

-- myOuterGaps :: l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Gaps l a
-- myOuterGaps = gaps [(U, 10), (D, 10), (L, 10), (R, 10)]

myLayouts :: ModifiedLayout AvoidStruts _ Window
myLayouts =
  desktopLayoutModifiers $
    smartBorders $
      emptyBSP
        ||| tabbedAlways shrinkText (theme gagboTheme) {fontName = myFont}
        ||| tallLayout
        ||| Mirror tallLayout
        ||| threeCol
  where
    tallLayout =
      renamed [Replace "Tall"] $ ResizableTall nmaster delta ratio []
    threeCol = renamed [Replace "ThreeCol"] $ magnifiercz' 1.3 $ ThreeColMid nmaster delta ratio
    nmaster = 1
    delta = 1.5 / 100
    ratio = toRational $ 2 / (1 + sqrt 5 :: Double)

gagboTheme :: ThemeInfo
gagboTheme =
  TI
    { themeName = "gagboTheme",
      themeAuthor = "gagbo",
      themeDescription = "gagbo's Theme",
      theme =
        def
          { inactiveBorderColor = paletteWhite gagboPalette,
            activeBorderColor = paletteYellow gagboPalette,
            activeColor = paletteBg gagboPalette,
            inactiveColor = paletteBlack gagboPalette,
            inactiveTextColor = paletteWhite gagboPalette,
            activeTextColor = paletteYellow gagboPalette,
            fontName = myFont,
            decoHeight = 20
          }
    }
