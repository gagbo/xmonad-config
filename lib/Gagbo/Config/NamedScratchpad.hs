-- |

module Gagbo.Config.NamedScratchpad
  ( module Gagbo.Config.NamedScratchpad
  , module XMonad.Util.NamedScratchpad
  ) where

import           XMonad
import           XMonad.Util.NamedScratchpad
import           XMonad.Hooks.ManageHelpers
import           XMonad.Hooks.FadeInactive      ( setOpacity )
import qualified XMonad.StackSet               as W

-- Scratchpads
scratchpads :: [NamedScratchpad]
scratchpads =
  [ NS "music"
       "alacritty --title Music -e ncmpcpp"
       (title =? "Music")
       (customFloating $ W.RationalRect (1 / 6) (1 / 20) (2 / 3) (2 / 3))
  , NS "term"
       "alacritty --title Scratchpad"
       (title =? "Scratchpad")
       (customFloating $ W.RationalRect (1 / 12) (1 / 20) (5 / 6) (2 / 3))
  , NS "volume"
       "pavucontrol"
       (className =? "Pavucontrol")
       (customFloating $ W.RationalRect (1 / 4) (1 / 20) (2 / 4) (2 / 4))
  , NS "KeePass"
       "keepassxc"
       (className =? "KeePassXC")
       (customFloating $ W.RationalRect (1 / 12) (1 / 20) (5 / 6) (2 / 3))
  , NS "Nextcloud"
       "nextcloud"
       (className =? "Nextcloud")
       (customFloating $ W.RationalRect (1 / 12) (1 / 20) (5 / 6) (2 / 3))
  ]

myNSManageHook :: NamedScratchpads -> ManageHook
myNSManageHook s = namedScratchpadManageHook s <+> composeOne
  [ title =? "Music" -?> (ask >>= \w -> liftX (setOpacity w 0.7) >> idHook)
  , title =? "Scratchpad" -?> (ask >>= \w -> liftX (setOpacity w 0.8) >> idHook)
  , className
  =?  "Pavucontrol"
  -?> (ask >>= \w -> liftX (setOpacity w 0.8) >> idHook)
  ]
