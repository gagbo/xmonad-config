-- |
module Gagbo.Config.Misc where

import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Prompt
import XMonad.Prompt.FuzzyMatch
import XMonad.Util.Loggers
import qualified Data.Map.Strict as Map
import Data.Maybe (fromJust)

myTerminal :: String
myTerminal = "alacritty"

myModKey :: KeyMask
myModKey = mod4Mask -- Use the "Win" key for the mod key

myConsoleMusicPlayer :: String
myConsoleMusicPlayer = "ncmpcpp"

myConsoleInfo :: String
myConsoleInfo = "htop"

myConsoleIRCClient :: String
myConsoleIRCClient = "weechat"

myScreenCap :: String
myScreenCap = "shotwell"

myDMenu :: String
myDMenu = "dmenu_run -h 35"

myRofi :: String
myRofi = "bash ~/.local/bin/rofiki"

myScreenLock :: String
myScreenLock = "xscreensaver-command -lock"

myFont :: String
myFont = "xft:Victor Mono:style=Regular:size=12"

myXPConfig :: XPConfig
myXPConfig =
  def
    { position = Top,
      alwaysHighlight = True,
      promptBorderWidth = 1,
      font = myFont,
      height = 35,
      bgColor = paletteBg gagboPalette,
      fgColor = paletteFg gagboPalette,
      bgHLight = paletteYellow gagboPalette,
      sorter = fuzzySort,
      searchPredicate = fuzzyMatch
    }

myXMobarPP :: PP
myXMobarPP =
  xmobarPP
    { ppTitle = cyan . shorten 70,
      ppCurrent = xmobarColor "#E5AC39" "" . wrap "[" "]",
      ppHiddenNoWindows = black,
      ppUrgent = xmobarColor "#E55C50" "#E5F4FF",
      ppHidden = blue . wrap "*" "", -- Hidden workspaces in xmobar
      ppSep = magenta " • ",
      ppTitleSanitize = xmobarStrip,
      ppOrder = \[ws, l, win, _wins] -> [ws, l, win],
      ppExtras = [logTitles formatFocused formatUnfocused]
    }
  where
    formatFocused = wrap (white "[") (white "]") . magenta . ppWindow
    formatUnfocused = wrap (lowWhite "[") (lowWhite "]") . blue . ppWindow

    ppWindow :: String -> String
    ppWindow = xmobarRaw . (\w -> if null w then "untitled" else w) . shorten 30

    blue, cyan, black, lowWhite, magenta, white :: String -> String
    magenta = xmobarColor "#ff79c6" ""
    cyan = xmobarColor "#2DACB7" ""
    blue = xmobarColor "#82AAFF" ""
    white = xmobarColor "#E5F4FF" ""
    lowWhite = xmobarColor "#bbbbbb" ""
    black = xmobarColor "#2D2D4C" ""

myWorkspaces :: [String]
myWorkspaces =
  let workspaceNames =
        ["dev", "www", "sys", "doc", "5", "chat", "mus", "vid", "9"]
   in [ show n ++ ":" ++ ws
        | (n, ws) <- zip [1 :: Integer .. 9] workspaceNames
      ]

myWorkspaceIndices :: Map.Map String Integer
myWorkspaceIndices = Map.fromList $ zip myWorkspaces [1..] -- (,) == \x y -> (x,y)

clickable :: [Char] -> [Char]
clickable ws = "<action=xdotool key super+"++show i++">"++ws++"</action>"
    where i = fromJust $ Map.lookup ws myWorkspaceIndices


-- Escape patterns for xmobar
xmobarEscape :: [Char] -> [Char]
xmobarEscape = concatMap doubleLts
  where
    doubleLts '<' = "<<"
    doubleLts x = [x]

data Palette = Pal
  { paletteBg :: String,
    paletteFg :: String,
    paletteBlack :: String,
    paletteRed :: String,
    paletteGreen :: String,
    paletteYellow :: String,
    paletteBlue :: String,
    paletteMagenta :: String,
    paletteCyan :: String,
    paletteWhite :: String
  }
  deriving (Show, Read)

instance Default Palette where
  def =
    Pal
      { paletteBg = "#000000",
        paletteFg = "#FFFFFF",
        paletteBlack = "#000000",
        paletteRed = "#AA0000",
        paletteGreen = "#00AA00",
        paletteYellow = "#AAAA00",
        paletteBlue = "#0000AA",
        paletteMagenta = "#AA00AA",
        paletteCyan = "#00AAAA",
        paletteWhite = "#FFFFFF"
      }

gagboPalette :: Palette
gagboPalette =
  def
    { paletteBg = "#0C120D",
      paletteFg = "#FCFCDD",
      paletteBlack = "#000000",
      paletteRed = "#FB5574",
      paletteGreen = "#CFFB6C",
      paletteYellow = "#E7D757",
      paletteBlue = "#7B97E2",
      paletteMagenta = "#FBBADC",
      paletteCyan = "#A6F5FC",
      paletteWhite = "#FCFBD2"
    }
