-- |

module Gagbo.Bindings.Qwerty
  ( keyBindings
  ) where

import           Gagbo.Bindings.Common
import           Gagbo.Config
import           System.Exit
import           XMonad
import           XMonad.Actions.Promote
import           XMonad.Actions.RotSlaves       ( rotSlavesDown
                                                , rotAllDown
                                                )
import           XMonad.Hooks.ManageDocks
import           XMonad.Hooks.UrgencyHook
import           XMonad.Layout.ToggleLayouts    ( ToggleLayout(..) )
import           XMonad.Layout.Gaps
import           XMonad.Prompt.Pass
import           XMonad.Prompt.ConfirmPrompt
import           XMonad.Prompt.Shell
import           XMonad.Prompt.XMonad
import qualified XMonad.StackSet               as W

keyBindings :: [(String, X ())]
keyBindings =
  [ ("M-S-q", confirmPrompt myXPConfig "exit" (io exitSuccess))
    , ("M-p"            , xmonadPrompt myXPConfig)
    , ("M-C-p"          , shellPrompt myXPConfig)
    , ("M-S-p"          , passPrompt myXPConfig)
    , ("M-b"            , sendMessage ToggleStruts) -- Make the windows go over status bars
    , ("M-f"            , sendMessage ToggleLayout) -- Toggle between full screen and others
    , ("M-<Tab>"        , sendMessage NextLayout)
    , ("M-<Space>"      , spawn myRofi)
    , ("M-r"            , spawn myDMenu)
    , ("M-<Backspace>"  , focusUrgent)
    , ("M-S-c"          , kill)

        -- Xmonad
    , ("M-q", spawn "xmonad --recompile; xmonad --restart")

        -- Windows navigation
    , ("M-j"            , windows W.focusDown)               -- Move focus to the next window
    , ("M-k"            , windows W.focusUp)                 -- Move focus to the prev window
    , ("M-S-m"          , windows W.swapMaster)            -- Swap the focused window and the master window
    , ("M-S-j"          , windows W.swapDown)              -- Swap the focused window with the next window
    , ("M-S-k"          , windows W.swapUp)                -- Swap the focused window with the prev window
    , ("M-S-<Backspace>", promote)                  -- Moves focused window to master, all others maintain order
    , ("M1-S-<Tab>"     , rotSlavesDown)              -- Rotate all windows except master and keep focus in place
    , ("M1-C-<Tab>"     , rotAllDown)                 -- Rotate all the windows in the current stack

        -- Gaps Control
    , ("M-C-g"          , sendMessage ToggleGaps)

        -- Lock screen
    , ("M-S-z"          , spawn myScreenLock)

        -- Terminal apps
    , ("M-<Return>"     , spawn myTerminal)
    , ("M-i", spawn $ myTerminal ++ " --class HTOPWindow -e " ++ myConsoleInfo)
    , ( "M-o"
      , spawn $ myTerminal ++ " --class IRCWindow -e " ++ myConsoleIRCClient
      )

        -- Scratchpads
    , ("<F12>", namedScratchpadAction scratchpads "term")
    , ("M-C-s", namedScratchpadAction scratchpads "volume")
    , ( "<F10>"
      , scratchpadGridSelect
        scratchpads
        [ ("Terminal" , "term")
        , ("Music"    , "music")
        , ("Volume"   , "volume")
        , ("KeePass"  , "KeePass")
        , ("NextCloud", "Nextcloud")
        ]
      )

        --- Grid Select
    , ( "M-S-t"
      , spawnSelected'
        [ ("Settings", "systemsettings5")
        , ("Kitty"   , "kitty")
        , ("Emacs"   , "emacs")
        , ("Firefox" , "firefox")
        , ("Kitty"   , "kitty")
        , ("Discord" , "Discord")
        , ("Keepass" , "keepassxc")
        ]
      )
    , ("M-S-g", goToSelected $ mygridConfig myColorizer)
    , ("M-S-b", bringSelected $ mygridConfig myColorizer)
    ]
    ++ commonKeyBindings
