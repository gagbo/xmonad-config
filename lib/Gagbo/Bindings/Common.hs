-- |

module Gagbo.Bindings.Common
  ( commonKeyBindings
  ) where

import           Control.Monad
import           Gagbo.Config
import           XMonad
import           XMonad.Actions.Volume

commonKeyBindings :: [(String, X ())]
commonKeyBindings =
  [
        -- Brightness Control
    ("<XF86MonBrightnessUp>", spawn "brightnessctl set 4%+")
  , ( "<XF86MonBrightnessDown>"
    , spawn "brightnessctl set 4%-"
    )

        -- Media keys
  , ("<XF86AudioLowerVolume>", setMute True >> lowerVolume 4 >> return ())
  , ("<XF86AudioRaiseVolume>", setMute True >> raiseVolume 4 >> return ())
  , ("<XF86AudioMute>"       , Control.Monad.void toggleMute)
  , ("<XF86AudioPlay>"       , spawn "mpc -q toggle")
  , ("<XF86AudioPause>"      , spawn "mpc -q pause")
  , ("<XF86AudioStop>"       , spawn "mpc -q stop")
  , ("<XF86AudioPrev>"       , spawn "mpc -q prev")
  , ("<XF86AudioNext>"       , spawn "mpc -q next")
  , ( "<XF86Music>"
    , spawn $ myTerminal ++ " --class MPCWindow -e " ++ myConsoleMusicPlayer
    )
  ]
