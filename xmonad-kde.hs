{-# OPTIONS_GHC -fno-warn-missing-signatures #-}

--------------------------------------------------------------------------------

-- | XMonad.hs
--
-- Gerry Agbobada's configuration file for xmonad using some of the latest recommended
-- features (e.g., 0.17 goodies such as ewmh and withEasySB).
module Main
  ( main,
  )
where

--------------------------------------------------------------------------------
import Data.Foldable (traverse_)
import Gagbo.Bindings.Qwerty (keyBindings)
import Gagbo.Config
  ( myLayouts,
    myManageHook,
    myModKey,
    myTerminal,
    myWorkspaces,
  )
import XMonad
import XMonad.Config.Kde
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.UrgencyHook
  ( UrgencyHook (..),
    withUrgencyHook,
  )
import qualified XMonad.StackSet as W
import XMonad.Util.EZConfig (additionalKeysP)
import qualified XMonad.Util.NamedWindows as W
import XMonad.Util.Run (safeSpawn)

--------------------------------------------------------------------------------

-- | Start xmonad using the main desktop configuration with
-- my overrides:
main :: IO ()
main = do
  xmonad $
    withUrgencyHook LibNotifyUrgencyHook $
        ewmhFullscreen $
          ewmh $
            kdeConfig
              { terminal = myTerminal,
                modMask = myModKey,
                normalBorderColor = "#0C0C14",
                focusedBorderColor = "#2DACB7",
                borderWidth = 2,
                manageHook = manageHook kdeConfig <+> myManageHook,
                workspaces = myWorkspaces,
                layoutHook = myLayouts
              }
              `additionalKeysP` keyBindings

-- original idea: https://pbrisbin.com/posts/using_notify_osd_for_xmonad_notifications/
data LibNotifyUrgencyHook = LibNotifyUrgencyHook
  deriving (Read, Show)

instance UrgencyHook LibNotifyUrgencyHook where
  urgencyHook LibNotifyUrgencyHook w = do
    winName <- W.getName w
    maybeIdx <- W.findTag w <$> gets windowset
    traverse_
      (\i -> safeSpawn "notify-send" [show winName, "workspace " ++ i])
      maybeIdx
