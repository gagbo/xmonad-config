{-# OPTIONS_GHC -fno-warn-missing-signatures #-}

--------------------------------------------------------------------------------

-- | XMonad.hs
--
-- Gerry Agbobada's configuration file for xmonad using some of the latest recommended
-- features (e.g., 0.17 goodies such as ewmh and withEasySB).
module Main
  ( main,
  )
where

--------------------------------------------------------------------------------
import Data.Foldable (traverse_)
import Gagbo.Bindings.Bepo (keyBindings)
import XMonad.Config.Bepo
import Gagbo.Config
  ( myLayouts,
    myManageHook,
    myModKey,
    myTerminal,
    myWorkspaces,
    myXMobarPP,
  )
import XMonad
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks (docks, manageDocks)
import XMonad.Hooks.StatusBar
  ( statusBarProp,
    withSB,
  )
import XMonad.Hooks.UrgencyHook
  ( UrgencyHook (..),
    withUrgencyHook,
  )
import qualified XMonad.StackSet as W
import XMonad.Util.EZConfig (additionalKeysP)
import qualified XMonad.Util.NamedWindows as W
import XMonad.Util.Run (safeSpawn)
import XMonad.Util.SpawnOnce (spawnAndDoOnce, spawnOnce)

--------------------------------------------------------------------------------

-- | Customize Startup
myStartupHook :: X ()
myStartupHook = do
  spawnOnce "picom -b"
  spawnOnce "dunst"
  spawnOnce
    "trayer --edge bottom --align right --SetDockType true --SetPartialStrut true --expand true --width 10 --transparent true --tint 0x5f5f5f --height 24"
  spawnOnce "udiskie"
  spawnOnce "nextcloud"
  spawnOnce "nm-applet --sm-disable --indicator"
  spawnOnce "blueman-applet"
  spawnAndDoOnce manageDocks "xmobar-laptop-a-bottom"
  spawnOnce "xfce4-power-manager"
  spawnOnce "xscreensaver -no-splash"

--------------------------------------------------------------------------------

-- | Start xmonad using the main desktop configuration with
-- my overrides:
main :: IO ()
main =
  xmonad $
    withUrgencyHook LibNotifyUrgencyHook $
      ewmhFullscreen $
        ewmh $
          docks $
            withSB
              (statusBarProp "xmobar-laptop-a-top" (pure myXMobarPP))
              $ bepoConfig
                { terminal = myTerminal,
                  modMask = myModKey,
                  normalBorderColor = "#0C0C14",
                  focusedBorderColor = "#2DACB7",
                  borderWidth = 2,
                  startupHook = myStartupHook,
                  manageHook = myManageHook,
                  workspaces = myWorkspaces,
                  layoutHook = myLayouts
                }
                `additionalKeysP` keyBindings

-- original idea: https://pbrisbin.com/posts/using_notify_osd_for_xmonad_notifications/
data LibNotifyUrgencyHook = LibNotifyUrgencyHook
  deriving (Read, Show)

instance UrgencyHook LibNotifyUrgencyHook where
  urgencyHook LibNotifyUrgencyHook w = do
    winName <- W.getName w
    maybeIdx <- W.findTag w <$> gets windowset
    traverse_
      (\i -> safeSpawn "notify-send" [show winName, "workspace " ++ i])
      maybeIdx
